package ifrn.pkg03_semestre.estrutura.dados.fila;

import java.util.Scanner;

public class Pilha {
   String A[] = new String[EstruturaDeDados.NUMERO_MAXIMO_DE_ELEMENTOS];
   int topo = 0;
   
   public void Push()
   {
       if(topo==EstruturaDeDados.NUMERO_MAXIMO_DE_ELEMENTOS){
           System.err.println("A pilha está cheia");
       }else{
       System.out.println("Digite o elemento a ser inserido na pilha: ");
       String cadeia = new Scanner(System.in).next();
       A[topo] = cadeia;
       topo++;
       }
   }
   
   public void Pop()
   {
       if(topo == 0) System.out.println("A pilha está vazia");
       else topo--;
   }
   
           // Método para Exibir elementos da Pilha
        public void Exibir() {
          
            System.out.print("Pilha: ");
          
            for (int i=0; i<topo; i++)
                System.out.print(A[i] + "  ");
          
            System.out.print("\n");
          
        }
 
 
        // Método para Remover todos elementos da Pilha
        public void Clear() {
          
            System.out.println("Pilha Limpa!");
            topo = 0;
  
        }
 
 
        // Método Gerenciador da Pilha
        public void play() {
  
            boolean Executar = true;
          
            System.out.println("\n======= MENU PILHA =======");
            System.out.println("1 - Inserir na Pilha");
            System.out.println("2 - Retirar da Pilha");
            System.out.println("3 - Limpar Pilha");
            System.out.println("4 - Exibir a Pilha");
            System.out.println("5 - Sair");
            System.out.println("============================");
          
            while (Executar == true) {
   
                int opcao = 0;
              
                while ( (opcao < 1) || (opcao > 5) ){
                  
                    System.out.print("Digite opcao: ");
                    opcao = new Scanner (System.in).nextInt();
                  
            }
  
                /* Chamando os métodos de acordo com
                 * a opção selecionada no Menu da Pilha*/
                if (opcao == 1) Push();
                if (opcao == 2) Pop();
                if (opcao == 3) Clear();
                if (opcao == 4) Exibir();
                if (opcao == 5) Executar = false;
              
                System.out.print("\n");
              
            }
  
       }
}
