/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifrn.pkg03_semestre.estrutura.dados.fila;

import java.util.Scanner;

/**
 *
 * @author Eduardo
 */
public class Fila {
    int extensao_da_fila = 0;
    String A[] = new String[EstruturaDeDados.NUMERO_MAXIMO_DE_ELEMENTOS];
      
      
        // Método para Inserir elementos na Fila
        public void InserirNaFila() {
 
            if (extensao_da_fila == EstruturaDeDados.NUMERO_MAXIMO_DE_ELEMENTOS)
               System.out.println("Fila Cheia");
 
            else {
 
               System.out.print("Digite o elemento a ser inserido na Fila: ");
               A[extensao_da_fila] = new Scanner (System.in).next();
               extensao_da_fila++;
          
            }
        }
      
      
        // Método para Remover elementos da Fila
        public void RemoverDaFila() {
 
            if (extensao_da_fila == 0)
               System.out.println("Fila Vazia: ");
 
            if (extensao_da_fila == 1)
               extensao_da_fila = 0;
 
            if (extensao_da_fila > 1) {
              
               for (int i=0; i < (extensao_da_fila - 1); ++i)
                    A[i] = A[i+1];
              
               extensao_da_fila--;
              
            }
        }
 
      
        // Método para Exibir elementos da Fila
        public void ExibirFila() {
          
            System.out.print("Fila: ");
          
            for (int i=0; i<extensao_da_fila;++i)
                System.out.print(A[i] + "  ");
          
            System.out.print("\n");
          
       }
 
      
        // Método para Retirar todos elementos da Fila
        public void LimparFila() {
          
            System.out.println("Fila Limpa!");
            extensao_da_fila = 0;
          
        }
 
      
        // Método Gerenciador da Fila
        public void play() {
  
            boolean executar = true;
          
            System.out.println("\n======= MENU  FILA =======");
            System.out.println("1 - Inserir na Fila: ");
            System.out.println("2 - Remover da Fila: ");
            System.out.println("3 - Exibir Fila: ");
            System.out.println("4 - Limpar Fila: ");
            System.out.println("5 - Sair ");
            System.out.println("============================");
          
            while (executar == true){
              
           int opcao = 0;
              
           while ( (opcao < 1) || (opcao > 5) ) {
                  
                    System.out.print("Digite opcao: ");
                    opcao = new Scanner (System.in).nextInt();
                  
          }
  
                /* Chamando os métodos de acordo com
                 * a opção selecionada no Menu da Fila*/
                if (opcao == 1) InserirNaFila();
                if (opcao == 2) RemoverDaFila();
                if (opcao == 3) ExibirFila();
                if (opcao == 4) LimparFila();
                if (opcao == 5) executar = false;
              
                System.out.print("\n");
              
            }
          
       }
      
    } 
