/* 
 * Thanks: Agradeço a DEUS pelo dom do conhecimento
 * Create on: 13/10/15
 */
package ifrn.pkg03_semestre.estrutura.dados.fila;

import java.util.Scanner;

public class EstruturaDeDados {

    static int NUMERO_MAXIMO_DE_ELEMENTOS = 1000;
    
    public EstruturaDeDados()
    {
        Pilha p = new Pilha();
        Fila f = new Fila();
        
        int tipoDeEstruturaDeDados = 0;
        
        while(tipoDeEstruturaDeDados !=3)
        {
            System.out.println("--------Menu Principal-------");
            System.out.println("1 - Pilha");
            System.out.println("2 - Fila");
            System.out.println("3 - Sair");
            System.out.println("----------------------------");
             
            System.out.print("Escolha o tipo de Estrutura de Dados: ");
            tipoDeEstruturaDeDados = new Scanner(System.in).nextInt();
            
            if(tipoDeEstruturaDeDados == 1) p.play();
            if(tipoDeEstruturaDeDados == 2) f.play();
            if(tipoDeEstruturaDeDados == 3) break;
            
            if(tipoDeEstruturaDeDados<1 || tipoDeEstruturaDeDados>3)
                System.out.println("Opção inválida");
        }
    }
}
