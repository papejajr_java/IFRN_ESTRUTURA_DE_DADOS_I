package atividade.fila;

public class Fila implements IFila
{
    private int capacidade;
    private Object F[]; //vetor usando Fila
    private int i = 0, length; //indice do elemento do topo da Fila

    public Fila(int tamanho)
    {
        capacidade = tamanho;
        F = new Object[capacidade];
    }
    
    @Override
    public int tamanho()
    {
        return length;
    }
    
    @Override
    public boolean vazia()
    {
        return(tamanho()==0);
    }
    
    @Override
    public Object proximo()
    {
        if(vazia()) System.out.println("Fila cheia!");
        return F[i];
    }
    
    @Override
    public void inserir(Object obj)
    {
        if(tamanho()== capacidade) System.out.println("Fila cheia!");
        F[i++] = obj;
    }

    @Override
    public Object retirar()
    {
        int j;
        Object aux = F[0];
        
        for(i=0;i<capacidade;i++)
        {
            j= i+1;
            if(j==capacidade)
            {
                F[capacidade-1] = null;
                break;
            } else
            {
                F[i] = F[j];
            }
        }
        return aux;
    }

    @Override
    public void exibir() {
        for(i=0;i<capacidade;i++)
        {
            if(F[i]!=null) System.out.print(F[i]+" ");
        }
        System.out.println("");
    }

    @Override
    public void furaFila(Object element) {
        Object aux[] = F;
        aux[0] = element;
        for(i=1;i<capacidade;i++)
        {
            
        }
    }
    
    
}