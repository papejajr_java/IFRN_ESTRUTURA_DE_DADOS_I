package atividade.fila;

public class QueueEmptyException  extends RuntimeException
{
    public QueueEmptyException(String erro)
    {
        super(erro);
    }
}