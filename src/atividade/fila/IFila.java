/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.fila;
public interface IFila {
    
	public int tamanho(); //retorna o numero de item na pilha
	public boolean vazia(); //retorna verdadeiro se a pilha estiver vazia, se não falso
	public Object proximo(); //retorna se remover o topo da lista
	public void inserir(Object element); //inserir um item na pilha
	public Object retirar(); // remove e retorna o item no topo da pilha
        public void exibir();
        public void furaFila(Object element);
}
