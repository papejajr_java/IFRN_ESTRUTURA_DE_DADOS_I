package atividade.fila;

public class TesteMain {
    
    public static void main(String[] args)
    {
        Fila fila = new Fila(10);

        fila.inserir(1);
        fila.inserir(2);
        fila.inserir(3);
        fila.inserir(4);
        fila.inserir(5);
        System.out.print("Elementos na fila: ");
        fila.exibir();

        System.out.print("Retirados da fila: ");
        System.err.println(fila.retirar());
        System.err.println(fila.retirar());
        
        System.out.print("Elementos na fila: ");
        fila.exibir();
        
    }
}
